import cv2,argparse,glob
import numpy as np
import imutils
import os

global flag 

def cv_size(img):
    return tuple(img.shape[1::-1])


def detect_lum(image, color):
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    ll = brightLAB[:,:,0]
    ll = ll.reshape(ll.shape[0]*ll.shape[1])
    mean = np.mean(ll)

    if color == "red":
        if mean > 90 and mean < 120:
            print("PASS")
        else:
            print("FAIL")
    elif color == "green":
        if mean > 160 and mean < 210:
            print("PASS")
        else:
            print("FAIL")
    elif color == "blue":
        if mean > 85  and mean < 95:
            print("PASS")
        else:
            print("FAIL")
    elif color == "white":
        if mean > 190  and mean < 240:
            print("PASS")
        else:
            print("FAIL")
          
    

def detect_red(image, minLAB, maxLAB):
     cv2.imshow('PRESS P for Previous, N for Next Image',image)
     brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
     maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
     resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
     # tresholding
     n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
     rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
     gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
     blurred = cv2.medianBlur(gray, 5)
     thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)  
     if(n - cv2.countNonZero(thresh)) > 0:
         print("red")
         detect_lum(image, "red")
         return False
     return True
        
             
def detect_blue(image, minLAB, maxLAB):
    cv2.imshow('PRESS P for Previous, N for Next Image',image)
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,9,2)
    #cv2.imshow("Blue thresh", thresh)  
    if (n - cv2.countNonZero(thresh)) > 0:
        print("blue")
        detect_lum(image, "blue")
        return False
    return True
        
             
def detect_green(image, minLAB, maxLAB):
    cv2.imshow('PRESS P for Previous, N for Next Image',image)
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    if (n - cv2.countNonZero(thresh)) > 0:
        print("green")
        detect_lum(image, "green")
        return False
    return True
        
def detect_white(image, minLAB, maxLAB):
    cv2.imshow('PRESS P for Previous, N for Next Image',image)
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    if (n - cv2.countNonZero(thresh)) > 2000 and (n - cv2.countNonZero(thresh)) != 0:
        print("white") 
        detect_lum(image, "white")
        return False
    return True
    

if __name__ == '__main__' :
    
    # Granice za crvenu boju u LAB modelu boja
    min_red_LAB = np.array([0, 135, 125])
    max_red_LAB = np.array([250, 200, 200])
    
    # Granice za zelenu boju u LAB modelu boja
    min_green_LAB = np.array([0, 50, 130])
    max_green_LAB = np.array([250, 120, 195])
    
    #Granice za plavu boju u LAB modelu boja
    min_blue_LAB = np.array([0, 140, 22])
    max_blue_LAB = np.array([250, 210, 104])
    
    #Granice za bijelu boju u LAB modelu boja
    min_white_LAB = np.array([0, 133, 70])
    max_white_LAB = np.array([255, 165, 136])
     
    global img
    global flag

    #files = glob.glob('test_images/*.png')
    files = glob.glob('test_images/*.png')
    files.sort()
    
    img = cv2.imread(files[0])
    n = cv_size(img)[0] // 2
    m = cv_size(img)[1] // 2
    img = img[n-200 : n+200, m-200 : m + 200]
    


    flag = True
    flag = detect_red(img, min_red_LAB, max_red_LAB)
    if flag:
        flag = detect_green(img, min_green_LAB, max_green_LAB)
    if flag:
        flag = detect_white(img, min_white_LAB, max_white_LAB)
    if flag:
        flag = detect_blue(img, min_blue_LAB, max_blue_LAB)
    flag = True
    i = 0
    while(True):
        k = cv2.waitKey(1) & 0xFF
        # Ako je korisnik pritisnio n, učitaj sljedeću sliku iz foldera
        if k == ord('n'):
            cv2.destroyAllWindows()
            i += 1
            print(i)
            img = cv2.imread(files[i%len(files)])
            n = cv_size(img)[0] // 2
            m = cv_size(img)[1] // 2
            img = img[n-200 : n+200, m-200 : m + 200]
            
          
            flag = detect_red(img, min_red_LAB, max_red_LAB)
            if flag:
                flag = detect_green(img, min_green_LAB, max_green_LAB)
            if flag:
                flag = detect_white(img, min_white_LAB, max_white_LAB)
            if flag:
                flag = detect_blue(img, min_blue_LAB, max_blue_LAB)
                
            flag = True
        # Ako je korisnik pritisnio p učitaj prethodnu sliku iz foldera
        elif k == ord('p'):
            cv2.destroyAllWindows()
            i -= 1
            print(i)
            img = cv2.imread(files[i%len(files)])
            n = cv_size(img)[0] // 2
            m = cv_size(img)[1] // 2
            img = img[n-200 : n+200, m-200 : m + 200]

            flag = detect_red(img, min_red_LAB, max_red_LAB)
            if flag:
                flag = detect_green(img, min_green_LAB, max_green_LAB)
            if flag:
                flag = detect_white(img, min_white_LAB, max_white_LAB)
            if flag:
                flag = detect_blue(img, min_blue_LAB, max_blue_LAB)
            flag = True
        elif k == 27:
            cv2.destroyAllWindows()
            break
            