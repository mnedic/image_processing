"""
Histograme LUM vrijednosti sam iscrtava za svaku boju zasebno, te ručno isčitavala rezultate. S obzirom na slike i LUM vrijednosti koje su zadane uzimala sam
kao prihvatiljivi raspon najviše vrijednosti koje su imale najveću frekvenciju po pixelima.

"""
# učitamo potrebne pakete
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import cv2,glob
import numpy as np
import os

# specificiramo boju za koju crtamo histogram
#color = 'pieces/'
color = 'PIECES1/red'

col = color.split('/')[1]

# želimo li histogram zumiran ili u cijeloj veličini
zoom = 0
# učitamo sve slike iz foldera
files = glob.glob(color + '*.PNG')
files.sort()
# prazni vektori za vrijednosti razdvojenih kanala za crtanje

H = np.array([])
S = np.array([])
V = np.array([])
Y = np.array([])
Cr = np.array([])
Cb = np.array([])
LL = np.array([])
LA = np.array([])
LB = np.array([])

# Iscrtavanje histograma

# dodamo vrijednost svakog kanala (iz svake slike) u odgovarajući vektor koji ga predtavlja 
for img in files[:]:
    # BGR
    im = cv2.imread(img)
    # HSV
    hsv = cv2.cvtColor(im,cv2.COLOR_BGR2HSV)
    h = hsv[:,:,0]
    h = h.reshape(h.shape[0]*h.shape[1])
    s = hsv[:,:,1]
    s = s.reshape(s.shape[0]*s.shape[1])
    v = hsv[:,:,2]
    v = v.reshape(v.shape[0]*v.shape[1])
    H = np.append(H,h)
    S = np.append(S,s)
    V = np.append(V,v)
    # YCrCb
    ycb = cv2.cvtColor(im,cv2.COLOR_BGR2YCrCb)
    y = ycb[:,:,0]
    y = y.reshape(y.shape[0]*y.shape[1])
    cr = ycb[:,:,1]
    cr = cr.reshape(cr.shape[0]*cr.shape[1])
    cb = ycb[:,:,2]
    cb = cb.reshape(cb.shape[0]*cb.shape[1])
    Y = np.append(Y,y)
    Cr = np.append(Cr,cr)
    Cb = np.append(Cb,cb)
    # Lab
    lab = cv2.cvtColor(im,cv2.COLOR_BGR2LAB)
    ll = lab[:,:,0]
    ll = ll.reshape(ll.shape[0]*ll.shape[1])
    la = lab[:,:,1]
    la = la.reshape(la.shape[0]*la.shape[1])
    lb = lab[:,:,2]
    lb = lb.reshape(lb.shape[0]*lb.shape[1])
    LL = np.append(LL,ll)
    LA = np.append(LA,la)
    LB = np.append(LB,lb)
    


plt.figure(figsize=[20,10])
plt.subplot(1,3,1)
plt.hist(LL, bins=255)
plt.title("L channel (LAB)")
plt.subplot(1,3,2)
plt.hist(Y, bins=255)
plt.title("Y channel (YCrCb)")
plt.subplot(1,3,3)
plt.title("V channel (HSV)")
plt.hist(V, bins=255)

#plt.savefig('pieces/result/LUM_histogram.png',bbox_inches='tight')
plt.savefig('PIECES1/result1/LUM_'+ col +'_histogram.png',bbox_inches='tight')
plt.show()

