"""
U ovom file nalaze se funkcije za detekciju boje testnih slika (detect_red, detect_blue, detect_green, detect_white). 
Granice pojedine boje ručno su isčitane iz histograma koji su dobiveni kao rezultat colorAnalys.py file-a. 

Funkcija detect_lum() određuje ima li slika prihvatiljivu vrijednost osvjetljenja. Granice su ručno određene iz histograma koji se dobiju kao rezultat LUMAnalys.py file-a.

Sva rješenje se ispisuju u result.txt file u obliku :naziv_fotografije.png : boja  PASS/FAIL (pri čemu PASS znaci da slika ima prihvatiljivo osvjetljenje, dok FAIL da je pala test osvjetljenja)

(Neke slike iz mape nemaju lum koji pise u nazivu slike, te se tu rjesenje ne podudara s onim sto piše u nazivu)
"""

import cv2,argparse,glob
import numpy as np
import imutils
import os

global flag 

filename = "rezultati/results.txt"

def cv_size(img):
    return tuple(img.shape[1::-1])


def detect_lum(image, color):
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    ll = brightLAB[:,:,0]
    ll = ll.reshape(ll.shape[0]*ll.shape[1])
    mean = np.mean(ll)

    if color == "red":
        if mean > 95 and mean < 140:
            with open(filename, "a") as f:
                f.write("PASS \n")
        else:
            with open(filename, "a") as f:
                f.write("FAIL \n")
    elif color == "green":
        if mean > 170 and mean < 230:
            with open(filename, "a") as f:
                f.write("PASS \n")
        else:
            with open(filename, "a") as f:
                f.write("FAIL \n")
    elif color == "blue":
        if mean > 85  and mean < 110:
            with open(filename, "a") as f:
                f.write("PASS \n")
        else:
            with open(filename, "a") as f:
                f.write("FAIL \n")
    elif color == "white":
        if mean > 200  and mean < 245:
            with open(filename, "a") as f:
                f.write("PASS \n")
        else:
            with open(filename, "a") as f:
                f.write("FAIL \n")
    

def detect_red(image, minLAB, maxLAB):
     brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
     maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
     resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
     # tresholding
     n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
     rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
     gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
     blurred = cv2.medianBlur(gray, 5)
     thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)  
     if(n - cv2.countNonZero(thresh)) > 0:
         with open(filename, "a") as f:
             f.write("red \n")
         detect_lum(image, "red")
         return False
     return True
        
             
def detect_blue(image, minLAB, maxLAB):
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,9,2)
    #cv2.imshow("Blue thresh", thresh)  
    if (n - cv2.countNonZero(thresh)) > 0:
        with open(filename, "a") as f:
             f.write("blue \n")
        detect_lum(image, "blue")
        return False
    return True
        
             
def detect_green(image, minLAB, maxLAB):
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    if (n - cv2.countNonZero(thresh)) > 0:
        with open(filename, "a") as f:
             f.write("green \n")
        detect_lum(image, "green")
        return False
    return True
        
def detect_white(image, minLAB, maxLAB):
    brightLAB = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    maskLAB = cv2.inRange(brightLAB, minLAB, maxLAB)
    resultLAB = cv2.bitwise_and(brightLAB, brightLAB, mask = maskLAB)
    # tresholding
    n = cv_size(resultLAB)[0]  * cv_size(resultLAB)[1]
    rgb = cv2.cvtColor(resultLAB, cv2.COLOR_LAB2RGB)
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    blurred = cv2.medianBlur(gray, 5)
    thresh = cv2.adaptiveThreshold(blurred ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    if (n - cv2.countNonZero(thresh)) > 2000 and (n - cv2.countNonZero(thresh)) != 0:
        with open(filename, "a") as f:
             f.write("white \n")
        detect_lum(image, "white")
        return False
    return True
    

if __name__ == '__main__' :
    
    #filename = "rezultati/results.txt"
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
            
    with open(filename, "w") as f:
        f.write("ISPISUJEM REZULTATE: \n")
    
    
    # Granice za crvenu boju u LAB modelu boja
    min_red_LAB = np.array([0, 135, 125])
    max_red_LAB = np.array([255, 220, 200])
    
    # Granice za zelenu boju u LAB modelu boja
    min_green_LAB = np.array([0, 50, 130])
    max_green_LAB = np.array([255, 120, 195])
    
    #Granice za plavu boju u LAB modelu boja
    min_blue_LAB = np.array([0, 140, 25])
    max_blue_LAB = np.array([255, 210, 104])
    
    #Granice za bijelu boju u LAB modelu boja
    min_white_LAB = np.array([0, 133, 70])
    max_white_LAB = np.array([255, 200, 140])
     
    global img
    global flag

    #files = glob.glob('test_images/*.png')
    files = glob.glob('test_images/*.png')
    files.sort()
    
    for i in range(len(files)-1):
        img = cv2.imread(files[i])
        n = cv_size(img)[0] // 2
        m = cv_size(img)[1] // 2
        img = img[n-200 : n+200, m-200 : m + 200]
    
        with open(filename, "a") as f:
            f.write(files[i] + ": ")
            
        flag = True
        flag = detect_red(img, min_red_LAB, max_red_LAB)
        if flag:
            flag = detect_green(img, min_green_LAB, max_green_LAB)
        if flag:
            flag = detect_white(img, min_white_LAB, max_white_LAB)
        if flag:
            flag = detect_blue(img, min_blue_LAB, max_blue_LAB)
        flag = True
   