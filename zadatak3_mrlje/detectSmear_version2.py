import cv2
import numpy as np 
import matplotlib.pyplot as plt
import glob
import numpy

def cv_size(img):
    return tuple(img.shape[1::-1]) 


def detect(path):
    #Bool-ova varijabla koja označava želimo prikaz među rezultata ili ne 
    show_result = 0
    #Učitavanje slike
    img = cv2.imread(path)
    #Resize slike na dimenziju za ispisivanje
    resized = cv2.resize(img, (cv_size(img)[0]//5, cv_size(img)[1]//5), interpolation = cv2.INTER_AREA)
    #U obzir uzimamo samo dio slike koji prikazuje screen
    resized = resized[160:530, 200:700]
    if show_result == 1:
        cv2.imshow("Image", resized)
        cv2.waitKey(0)
    
    #Sliku iz RBG prebacujemo u grayscale
    gray_image = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    if show_result == 1:
        cv2.imshow("Gray image", gray_image)
        cv2.waitKey(0)

    
    eq_img = cv2.equalizeHist(gray_image)
    if show_result == 1:
        cv2.imshow("EQ image", eq_img)
        cv2.waitKey(0)
    
    
    thresh_img = cv2.adaptiveThreshold(eq_img ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV,59,7)
    rez = cv2.bitwise_not(thresh_img)
    if show_result == 1:
        cv2.imshow("Thresh", rez)
        cv2.waitKey(0)
    
   
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
    blob = cv2.morphologyEx(rez, cv2.MORPH_OPEN, kernel)
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (12,12))
    blob = cv2.morphologyEx(blob, cv2.MORPH_CLOSE, kernel)

    if show_result == 1:
        cv2.imshow("Blob",blob)
        cv2.waitKey(0)
    
    
    edge1 = cv2.Canny(blob,170,400)             
    edge = edge1
    if show_result == 1:
       cv2.imshow("Edge Detection",edge)
       cv2.waitKey(0)
    
    cnts,hier = cv2.findContours(edge, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(resized,cnts,-1,(0,255,255),2)
    if show_result == 1:
       cv2.imshow("Cont",resized) 
       cv2.waitKey(0)
                                
    list1=[]
    mask_img = numpy.zeros((370,500,1),numpy.float)
          
    oimg = cv2.imread(path)
    oimg = cv2.resize(oimg, (cv_size(oimg)[0]//5, cv_size(oimg)[1]//5), interpolation = cv2.INTER_AREA)
    oimg = oimg[160:530, 200:700]              
            
    for c in cnts:
        p = cv2.arcLength(c,True)
        approx = cv2.approxPolyDP(c,0.02*p,True)
        
        (x,y),radius = cv2.minEnclosingCircle(c)
        radius = int(radius)
        if abs(cv2.contourArea(c)-(3.14*(radius**2))) <445 and cv2.contourArea(c)>950:
            cv2.drawContours(oimg,[approx],-1,(0,0,255),2)
            cv2.drawContours(mask_img,[approx],-1,(255,255,255),-1)               
            list1.append(c)
            
    cv2.imshow("FinalResult",oimg)
    cv2.waitKey(0)
    if show_result == 1:         
        cv2.imshow("Mask",mask_img)       
        cv2.waitKey(0)
    cv2.destroyAllWindows();   

dirr = "Dirty/*.png"
#dirr = "Clean/*png"

for image in glob.glob(dirr):
    print(image)
    detect(image)

#detect('Dirty/0_white_OE1946L011.png')
