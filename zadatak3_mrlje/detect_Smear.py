import cv2
from matplotlib import pyplot as plt
import numpy
import glob
import imutils
from imutils import contours
from skimage import measure



directory = "Dirty/*.png"
images = glob.glob(directory)

def detect_Smear(file):
    #Učitavanje slike
    a = cv2.imread(file)
    #Resize slike, ali da ostane u istom omjeru
    b = imutils.resize(a,width=800)
    print("Image " + file.split("\\")[1] + " :")
      
         
    img1 = cv2.GaussianBlur(b,(3,3),0)
    imarr = numpy.array(img1, dtype=numpy.float)
    avg_img = numpy.array(numpy.round(imarr),dtype=numpy.uint8)
    #Bool avg označava hoćemo li prikazati među rezultate ili ne 
    avg = 0      
    if avg == 1:
        cv2.imshow("Average",avg_img)
        cv2.waitKey(0)
        
    #Gray scale slika
    avg_img_grey = cv2.cvtColor(avg_img,cv2.COLOR_BGR2GRAY)
    #thresh_img = cv2.threshold(avg_img_grey, 226, 255, cv2.THRESH_BINARY)[1]
    thresh_img = cv2.adaptiveThreshold(avg_img_grey ,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY_INV,11,2)
    thresh_img = cv2.threshold(avg_img_grey, 226, 255, cv2.THRESH_BINARY_INV)[1]  ##226 
                                      
    if avg==1:
         cv2.imshow("Threshold Gaussian",thresh_img)      
         cv2.waitKey(0)
         
    edge1 = cv2.Canny(thresh_img,75,100)             
    edge = edge1
    if avg==1:
       cv2.imshow("Edge Detection",edge)
       cv2.waitKey(0)
    
    cnts,hier = cv2.findContours(edge, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(avg_img,cnts,-1,(0,255,255),2)
    if avg==1:
       cv2.imshow("Cont",avg_img) 
       cv2.waitKey(0)
                                
    #cv2.drawContours(avg_img,cnts,-1,(0,255,0),3);
    list1=[]
    mask_img = numpy.zeros((800,600,1),numpy.float)
                        
    oimg=cv2.imread(file)
    oimg=imutils.resize(oimg,width=800,height=500)            
    for c in cnts:
        p = cv2.arcLength(c,True)
        approx = cv2.approxPolyDP(c,0.02*p,True)
        
        (x,y),radius=cv2.minEnclosingCircle(c)
        radius = int(radius)
        if abs(cv2.contourArea(c)-(3.14*(radius**2))) <500 and cv2.contourArea(c)>500:
            cv2.drawContours(oimg,[approx],-1,(0,0,255),2)
            cv2.drawContours(mask_img,[approx],-1,(255,255,255),-1)               
            list1.append(c)
            print("Ima mrlju!")
    
    cv2.imshow("FinalResult",oimg)
    cv2.waitKey(0)
    if avg == 1:        
        cv2.imshow("Mask",mask_img)       
               
        cv2.waitKey(0)
        cv2.imwrite("FinalResult_" + file + ".png",oimg)       
    cv2.destroyAllWindows();  
    
#detect_Smear("Dirty\\0_white_OE1945L014.png")  
    
for image in images:
    detect_Smear(image)