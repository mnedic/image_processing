from skimage import data, io, filters
from skimage.color import rgb2gray
from skimage.color import rgb2hsv
import matplotlib.pyplot as plt
from skimage.exposure import  rescale_intensity


image = data.coins()
edges = filters.sobel(image)
io.imshow(edges)
io.show()
im = data.astronaut()
print(im)
image2 = rgb2gray(data.astronaut())
print(image2)
edges2 = filters.sobel(image2)
io.imshow(edges2)
io.show()
img1 = rgb2gray(data.astronaut())

# Primjer promjena kanala boja
rgb_img = data.coffee()
hsv_img = rgb2hsv(rgb_img)
hue_img = hsv_img[:, :, 0]
value_img = hsv_img[:, :, 2]

fig, (ax0, ax1, ax2) = plt.subplots(ncols=3, figsize=(8, 2))

ax0.imshow(rgb_img)
ax0.set_title("RGB image")
ax0.axis('off')
ax1.imshow(hue_img, cmap='hsv')
ax1.set_title("Hue channel")
ax1.axis('off')
ax2.imshow(value_img)
ax2.set_title("Value channel")
ax2.axis('off')

fig.tight_layout()
#%%
# convert to grayscale

original = data.astronaut()
grayscale = rgb2gray(original)

fig, axes = plt.subplots(1, 2, figsize=(8, 4))
ax = axes.ravel()

ax[0].imshow(original)
ax[0].set_title("Original")
ax[1].imshow(grayscale, cmap=plt.cm.gray)
ax[1].set_title("Grayscale")

fig.tight_layout()
plt.show()



#%%
suma = value_img + hue_img
ax = axes.ravel()

ax[0].imshow(suma)
ax[0].set_title("Sum")

fig.tight_layout()
plt.show()

rescale_intensity(rgb_img)