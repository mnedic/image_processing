from imutils import contours
from skimage import measure
import numpy as np
import argparse
import imutils
import skimage.io
import matplotlib.pyplot as plt
from skimage.color import rgb2gray
from skimage.filters import gaussian, threshold_mean, threshold_otsu, threshold_li, threshold_local, threshold_yen
import cv2
from skimage.transform import rescale, resize

#Image on which algoritam works
image = skimage.io.imread("preuzmi.jpg")
#image = skimage.io.imread("image3.jpg")
#image = resize(image, (np.shape(image)[0]//3, np.shape(image)[1]//3))


# Image on which algoritam doesn't work 
#image = skimage.io.imread("lightbulbs.jpg")
#image = resize(image, (np.shape(image)[0]//2, np.shape(image)[1]//2))

#image = skimage.io.imread("image2.jpg")


#%%

gray = rgb2gray(image)
blurred = gaussian(gray, sigma=1)
plt.imshow(blurred, cmap=plt.cm.gray)
plt.show()

#%%

# threshold the image to reveal light regions in the
# blurred image
thresh = threshold_otsu(blurred)
binary = blurred > thresh
plt.imshow(binary, cmap=plt.cm.gray)
plt.show()


#%%

from skimage.morphology import erosion, dilation, diamond

binary = erosion(binary, diamond(7))
binary = dilation(binary, diamond(7))

plt.imshow(binary, cmap=plt.cm.gray)
plt.show()

#%%

# perform a connected component analysis on the thresholded
# image, then initialize a mask to store only the "large"
# components
labels = measure.label(binary, neighbors=4, background=0)
mask = np.zeros(binary.shape, dtype="uint8")
# loop over the unique components
for label in np.unique(labels):
	# if this is the background label, ignore it
	if label == 0:
		continue
	# otherwise, construct the label mask and count the
	# number of pixels 
	labelMask = np.zeros(binary.shape, dtype="uint8")
	labelMask[labels == label] = 255
	numPixels = np.sum(labelMask == 255)
	# if the number of pixels in the component is sufficiently
	# large, then add it to our mask of "large blobs"
	if numPixels > 300:
		mask = cv2.add(mask, labelMask)

# find the contours in the mask, then sort them from left to
# right
cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = contours.sort_contours(cnts)[0]
# loop over the contours
for (i, c) in enumerate(cnts):
	# draw the bright spot on the image
	(x, y, w, h) = cv2.boundingRect(c)
	((cX, cY), radius) = cv2.minEnclosingCircle(c)
	cv2.circle(image, (int(cX), int(cY)), int(radius),(0, 0, 255), 3)
	cv2.putText(image, "#{}".format(i + 1), (x, y - 15),cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
# show the output image
cv2.imshow("Image", image)
cv2.waitKey(0)