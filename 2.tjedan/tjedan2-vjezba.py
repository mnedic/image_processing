from skimage import data
from skimage.filters import threshold_mean, try_all_threshold, threshold_minimum, threshold_otsu, threshold_local
import matplotlib.pyplot as plt
#%%
### PRIMJER THRESHOLDINGA 
image = data.camera()
tresh = threshold_mean(image)
binary = image > tresh

fig, axes = plt.subplots(ncols=2, figsize=(8, 3))
ax = axes.ravel()

ax[0].imshow(image, cmap=plt.cm.gray)
ax[0].set_title("Orginal image")

ax[1].imshow(binary, cmap=plt.cm.gray)
ax[1].set_title("Results")

for a in ax:
    a.axis('off')
    
plt.show()

#%%

page = data.page()
fig, ax = try_all_threshold(page,figsize=(10, 8), verbose=False )
plt.show()

#%%

thresh_min = threshold_minimum(image)
binar = image > thresh_min

fig, ax = plt.subplots(2,2,figsize=(10,10))

ax[0,0].imshow(image, cmap=plt.cm.gray)
ax[0,0].set_title("Original")

ax[0,1].hist(image.ravel(), bins=256)
ax[0,1].set_title("Histogram")

ax[1,0].imshow(binar, cmap=plt.cm.gray)
ax[1,0].set_title("Threshholded (min")

ax[1,1].hist(image.ravel(), bins=256)
ax[1,1].axvline(thresh_min, color="r")

plt.show()

#%%

global_thresh = threshold_otsu(page)
binary_global = page > global_thresh

block_size = 35
adaptive_thresh = threshold_local(page, block_size, offset=10)
binary_local = page > adaptive_thresh

fig, axes = plt.subplots(nrows=3, figsize=(7,8))
ax = axes.ravel()
plt.gray()

ax[0].imshow(page)
ax[0].set_title("Original")

ax[1].imshow(binary_global)
ax[1].set_title("Threshed global")

ax[2].imshow(binary_local)
ax[2].set_title("Theshed localy")

plt.show()

#%%

from skimage import filters
import numpy as np
# regionprops

coins = data.coins()
plt.imshow(coins, cmap = "gray")
coins_denoised = filters.median(coins, selem=np.ones((5,5)))

f, ax = plt.subplots(1,2, figsize=(15,5))

ax[0].imshow(coins)
ax[1].imshow(coins_denoised)


#%%
from skimage import feature

# u ovisnosti o parametru sigma imamo više ili manje rubova
edges = feature.canny(coins, sigma=1)
plt.imshow(edges)

edges = feature.canny(coins, sigma=3)
plt.imshow(edges)

#%%
from scipy.ndimage import distance_transform_edt
# Minus ili inverz od edges se piše sa ~ a ne sa - 
dt = distance_transform_edt(~edges)

plt.imshow(dt)

#%%

local_max = feature.peak_local_max(dt,indices=False, min_distance=5)
plt.imshow(local_max, cmap="gray")
#%%

peak_idx = feature.peak_local_max(dt, indices=True, min_distance=5)
print(peak_idx[:5])

plt.plot(peak_idx[:,1], peak_idx[:,0], "r.")
plt.imshow(dt)
#%%

from skimage import measure

markers = measure.label(local_max)

from skimage import morphology, segmentation

labels = morphology.watershed(-dt, markers)
plt.imshow(segmentation.mark_boundaries(coins, labels))

#%%
from skimage import color 
plt.imshow(color.label2rgb(labels, image=coins))

#%%

plt.imshow(color.label2rgb(labels, image=coins, kind="avg"), cmap="gray")

#%%

regions = measure.regionprops(labels, intensity_image=coins)

region_means = [r.mean_intensity for r in regions]
plt.hist(region_means, bins=20)

#%%

from sklearn.cluster import KMeans

model = KMeans(n_clusters=2)

region_means = np.array(region_means).reshape(-1,1)
model.fit(np.array(region_means).reshape(-1,1))
print(model.cluster_centers_)

#%%

bg_fg_labels = model.predict(region_means)
print(bg_fg_labels)

#%%
classified_labels = labels.copy()
for bg_fg, region in zip(bg_fg_labels, regions):
    classified_labels[tuple(region.coords.T)] = bg_fg

plt.imshow(color.label2rgb(classified_labels, image=coins))