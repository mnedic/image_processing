# -*- coding: utf-8 -*-
"""
Created on Sat May  2 17:50:20 2020

@author: lenan
"""

import skimage.io 
import skimage.viewer
from skimage import data
from skimage.filters import threshold_mean, try_all_threshold, threshold_minimum, threshold_otsu, threshold_local
import matplotlib.pyplot as plt
import skimage.color
import numpy as np
#%%

image = skimage.io.imread(fname="my_photo.jpg")

viewer = skimage.viewer.ImageViewer(image)
viewer.show()

#resize

new_shape = (image.shape[0] // 4, image.shape[1]//4, image.shape[2])
small = skimage.transform.resize(image= image, output_shape=new_shape)
viewer = skimage.viewer.ImageViewer(small)
viewer.show()

new_shape2 = (image.shape[0] * 2, image.shape[1] *2, image.shape[2])
big  = skimage.transform.resize(image= image, output_shape=new_shape2)
viewer = skimage.viewer.ImageViewer(big)
viewer.show()


#%%

gray_image = skimage.color.rgb2gray(image)
#viewer = skimage.viewer.ImageViewer(gray_image)
print(gray_image.shape)

#%%
block_size = 3
adaptive_thresh = threshold_local(gray_image, block_size, offset=1)
binary_local = gray_image > adaptive_thresh

global_thresh = threshold_otsu(gray_image)
binary_global = gray_image > global_thresh


fig, axes = plt.subplots(ncols=3, figsize=(14,15))
ax = axes.ravel()

ax[0].imshow(gray_image)
ax[0].set_title("Original")


ax[1].imshow(binary_local)
ax[1].set_title("Theshed localy")

ax[2].imshow(binary_global)
ax[2].set_title("Theshed globaly")


plt.show()

#%%












