# -*- coding: utf-8 -*-
"""
Created on Sun May  3 13:08:43 2020

@author: lenan
"""

### učitavanje foldera slika 

import os
import skimage.io 
import skimage.viewer
from skimage import data
from skimage.filters import threshold_mean, try_all_threshold, threshold_minimum, threshold_otsu, threshold_local
import matplotlib.pyplot as plt
import skimage.color
import numpy as np

def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = skimage.io.imread(os.path.join(folder,filename))
        if img is not None:
            images.append(img)
    return images

#učita slike iz foldera
images = load_images_from_folder("Tjedan 2 - slike")

# iscrta 21 sliku iz foldera
viewer = skimage.viewer.ImageViewer(images[20])
viewer.show()


#%%
## Iscrta sve slike iz foldera
for img in images:
    viewer = skimage.viewer.ImageViewer(img)
    viewer.show()

#%%

from skimage.io import imread_collection

#putanja do foldera
col_dir = 'Tjedan 2 - slike/*.png'

#kreira kolekciju s dostupnim slikama iz foldera
col = imread_collection(col_dir)

print(len(col))
viewer = skimage.viewer.ImageViewer(col[46])
viewer.show()

#%%

import errno

filename = "results/results.txt"
if not os.path.exists(os.path.dirname(filename)):
    try:
        os.makedirs(os.path.dirname(filename))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

with open(filename, "w") as f:
    f.write("Ispisujem rezultate")

#%%

def thrash_global_vs_local(image, i):
    hsv_img = skimage.color.rgb2hsv(image)
    gray_img = skimage.color.rgb2gray(image)

    with open(filename, "w") as f:
        f.write("HSV image:" + str(hsv_img[:,:,0]))
        
    block_size = 5
    adaptive_thresh = threshold_local(gray_img, block_size, offset=1)
    binary_local = gray_img > adaptive_thresh

    global_thresh = threshold_otsu(gray_img)
    binary_global = gray_img > global_thresh

    fig, axes = plt.subplots(ncols=3, figsize=(14,15))
    ax = axes.ravel()

    ax[0].imshow(gray_img)
    ax[0].set_title("Original")

    ax[1].imshow(binary_local)
    ax[1].set_title("Theshed localy")

    ax[2].imshow(binary_global)
    ax[2].set_title("Theshed globaly")

    plt.savefig("results/thrash_globalVSlocal" + str(i) + ".png")    

for i in range(len(col)):
    thrash_global_vs_local(col[i-1], i-1)



